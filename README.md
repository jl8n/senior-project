# senior-project

## Intro

> At a minimum you will need to following to fully run this project:
> - ESP32 microcontroller
> - BME280 sensor
> - Raspberry Pi (or other Linux server)

## Install Client Dependencies
    cd senior-project/web
    npm i
    npm install -g @quasar/cli


## Start Client Dev Server
    cd senior-project/web
    quasar dev


## Build Client
    cd senior-project/web
    quasar build
> Files placed in `senior-project/web/dist`

_____

## Install Server Dependencies
    cd senior-project/server
    npm i


## Start Server
> Does not need building

    cd senior-project/web
    node index.js

_____

## Build Grow Server
> - This code needs to be built using the same architecture as the device it's intended to run on.  
> - It's easiest to just build it on the Raspberry Pi if that's what you intend to use.  
> - You will want to modify the code, specifically the IP's and InfluxDB configuration, before building this part of the project.

    cd senior-project/sensors/rust-server
    cargo build --release

> `cargo run` can alternatively be used to start a development server without optimizations  

## Start Grow Server
    cd senior-project/sensors/rust-server/target/dist
    ./rust-server

_____

## Build Grow Unit
> - The [Arduino IDE](https://www.arduino.cc/en/software) is required to build and flash this portion of the project.  
> - You will need to modify the code before flashing each ESP32 so that each is assigned a unique `Plant Number` as well as the IP address of the Raspberry Pi.


1. Plug your ESP32 into your computer via USB
2. Open the following file in the Arduino IDE  
`senior-project/sensors/client/growunit.ino`

3. Flash the code onto the ESP32 using the Arduino IDE  


const dbConfig = require('./db');
const cron = require('node-cron');
const sqlite3 = require('sqlite3');
const nodemailer = require('nodemailer');
const cors = require('cors');
const express = require('express');
const app = express();


const PORT = 3000;

/* middleware */
app.use(cors());
app.use(express.json());


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*'); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.options('*', cors());


const cToF = (celsius) => {
  return (celsius * 9 / 5) + 32;
};


app.get('/email', async (req, res) => {

});

/* ===== Alerts ===== */


cron.schedule('* * * * *', async () => {
  // get config min & max values
  const db = new sqlite3.Database('./config.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      return console.error(err.message);
    }
  });

  db.serialize(() => {
    db.each(`SELECT * FROM ${dbConfig.table}`, async (err, row) => {

      const query = `
        from(bucket:"${dbConfig.bucket}")
        |> range(start: -5m)
        |> filter(fn: (r) =>
          r._measurement == "temperature" or
          r._measurement == "humidity" or
          r._measurement == "pressure"
        )
        |> last()
      `;

      let data = await dbConfig.queryApi.collectRows(query);
      let message = '';

      data.forEach(key => {
        
        if (key._measurement == 'temperature') {
          console.log(key._measurement, cToF(key._value), row.tempMin, row.tempMax);
          if (cToF(key._value) < row.tempMin) {
            message += `Temperature too small (${cToF(key._value)})\n`;
          } else if (cToF(key._value) > row.tempMax) {
            message += `Temperature too big (${cToF(key._value)})\n`;
          }
        } else if (key._measurement == 'humidity') {
          console.log(key._measurement, key._value, row.humidMin, row.humidMax);
          if (key._value < row.humidMin) {
            message += `Humidity too small (${key._value})\n`;
          } else if (key._value > row.humidMax) {
            message += `Humidity too big(${key._value})\n`;
          }
        }
      });

      let transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: "860bf5a6416e42",
          pass: "e11b485fcbd896"
        }
      });

      let info = await transporter.sendMail({
        from: '"me@growcenter.com',
        to: "notify@growcenter.com",
        subject: "Your plants may be in danger",
        text: `You need to check on your plants. ${message}`,
        html: `<b>You need to check on your plants. ${message}</b>`
      });

      console.log("Message sent: %s", info.messageId);
    });
  });

  db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
  });
  res.sendStatus(200);
});


/* ===== API =====  */
app.post('/', (req, res) => { 
  console.log(req.body);
  res.end();
});

app.get('/', (req, res) => { 
  console.log("GET /"); 
  res.sendStatus(200);
});

app.get('/config', (req, res) => {

  const db = new sqlite3.Database('./config.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
  });

  db.serialize(() => {
    db.each(`SELECT * FROM ${dbConfig.table}`, (err, row) => {
      if (err) {
        console.error(err.message);
      }
      console.log(row);
      res.send(row);
    });
  });

  db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Close the database connection.');
  });
 //res.send(config);

});



app.post('/config', (req, res) => {
  console.log('POST /config', req.body);
  const db = new sqlite3.Database('./config.db', (err) => {
    if (err)
      return console.error(err.message);
  });

  db.serialize(() => {
    const inputData = [
      req.body.tempMin,
      req.body.tempMax,
      req.body.humidMin,
      req.body.humidMax,
      req.body.lightsOn,
      req.body.lightsOff
    ];

    db.run(
    `
      UPDATE ${dbConfig.table}
      SET 
        tempMin = ?,
        tempMax = ?,
        humidMin = ?,
        humidMax = ?,
        lightsOn = ?,
        lightsOff = ?
      WHERE id = 1;
    `,
    inputData, (err, rows) => {
      if (err) {
        res.status(400);
        res.send({ error: err });
      }
    });
    res.sendStatus(200);  // OK
  });
});

app.get('/api', async (req, res) => {
  console.log('GET /api');
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -6h)
  `;
  let data = await dbConfig.queryApi.collectRows(query)
    .then(response => {
      //response.forEach(x => console.log(JSON.stringify(x)))
      return response;
    });

  res.json(data);
});


app.get('/api/latest', async (req, res) => {
  console.log('GET /api/latest');
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -5m)
    |> filter(fn: (r) =>
      r._measurement == "temperature" or
      r._measurement == "humidity" or
      r._measurement == "pressure"
    )
    |> last()
  `;

  let data = await dbConfig.queryApi.collectRows(query);
  if (data == {})
    res.sendStatus(204);
  console.log(data, typeof(data));
  res.json(data);
});

app.get('/api/time/:time', async (req, res) => {
  console.log('GET /api/time');
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -${req.params.time})
    |> filter(fn: (r) =>
      r._measurement == "temperature" or
      r._measurement == "humidity" or
      r._measurement == "pressure"
    )
  `;

  let data = await dbConfig.queryApi.collectRows(query);
  if (data == {})
    res.sendStatus(204);
  
  data.forEach(key => {
    delete key.result;
    delete key.table;
    delete key._start;
    delete key._stop;
    delete key._field;
  });

  //console.log(data, typeof (data));
  res.json(data);
});

app.get('api/measurements/:measurement', async (req, res) => {
  console.log('GET /api/latest');
});

app.get('/plants/:plant/:', async (req, res) => {
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -5m)
    |> filter(fn: (r) =>
      r.plant == "${req.params.plant}"
    )
  `;
  let data = await dbConfig.queryApi.collectRows(query)
  .then(response => {
    //response.forEach(x => console.log(JSON.stringify(x)))
    return response;
  });
  
  res.json(data);
  /*
  queryApi.queryRows(query, {
    next(row, tableMeta) {
      const o = tableMeta.toObject(row);
      console.log(o);
      res.json(o);
    },
    error(error) {
      console.error(error);
      console.log('\\nFinished ERROR');
      res.sendStatus(500);
    }
  });
  */
  //res.sendStatus(404);
  //res.send('Hello World!');
});



app.get('/plants/:plant/temperature', async (req, res) => {
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -5m)
    |> filter(fn: (r) =>
      r.plant == "${req.params.plant}" and
      r._measurement == "temperature"
    )
  `;
  let data = await dbConfig.queryApi.collectRows(query); 
  res.json(data);
});


app.get('api/temperature', async (req, res) => {
  const query = `
    from(bucket:"${dbConfig.bucket}")
    |> range(start: -5m)
    |> filter(fn: (r) =>
      r.plant == "${req.params.plant}" and
      r._measurement == "temperature"
    )
  `;
  let data = await dbConfig.queryApi.collectRows(query); 
  res.json(data);
});

let server = app.listen(PORT, function(err){ 
  if (err) console.log(err); 
  console.log("Server listening on PORT", PORT); 
});









  /*
app.use((req, res, next) => {
    const start = Date.now()
  
    res.on('finish', () => {
      const duration = Date.now() - start
      console.log(`Request to ${req.path} took ${duration}ms`);
  
      influx.writePoints([
        {
          measurement: 'response_times',
          tags: { host: os.hostname() },
          fields: { duration, path: req.path },
        }
      ]).catch(err => {
        console.error(`Error saving data to InfluxDB! ${err.stack}`)
      })
    })
    return next()
  })
  
  app.get('/', function (req, res) {
    setTimeout(() => res.end('Hello world!'), Math.random() * 500)
  })
  
  app.get('/times', function (req, res) {
    influx.query(`
      select * from response_times
      where host = ${Influx.escape.stringLit(os.hostname())}
      order by time desc
      limit 10
    `).then(result => {
      res.json(result)
    }).catch(err => {
      res.status(500).send(err.stack)
    })
  })

*/


/*


*/
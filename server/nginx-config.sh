#!/bin/bash

#read -p "Purge nginx and reinstall? (y/n) " is_reinstall

#if [[ "$reinstall" == "y" || "$reinstall" == "Y" ]]; then
#    apt-get purge nginx nginx-common nginx-full
#    apt-get install nginx
#fi

read -p "Enter project name: " project
read -p "Enter root directory: " root
#read -p "Would you like to enable history mode? " is_history
touch "/var/log/nginx/$project-access.log"
touch "/var/log/nginx/$project-error.log"

echo  "server {
    listen 80;
    server_name $project;
    root $root;

    access_log /var/log/nginx/$project-access.log;
    error_log /var/log/nginx/$project-error.log;

    # enables history mode for vue-router
    location / {
        try_files $uri $uri/ /index.html;
    }
}" > "/etc/nginx/sites-available/$project"

ln -s /etc/nginx/sites-available/$project /etc/nginx/sites-enabled/$project
service nginx restart
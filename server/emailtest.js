const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function main () {
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
            user: "860bf5a6416e42",
            pass: "e11b485fcbd896"
        }
    });

    transporter.verify(function (error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log('Server is ready to take our messages');
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"me@growcenter.com', // sender address
        to: "notify@growcenter.com", // list of receivers
        subject: "Your plants may be in danger", // Subject line
        text: "Hello world?", // plain text body
        html: "<b>Hello world?</b>", // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);
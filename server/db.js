/* InfluxDB */
const { InfluxDB } = require('@influxdata/influxdb-client');
const token = '86F7MkXKpbZDGm9pe7tPWfO1yWd4dpqWdD3NV3wMmMhj_z0I45dYEq2tsxCAqSsRzksfNLdFZxFfPY38XHSW-w==';
const org = 'e3455fb6e26d89b3';
const bucket = 'growdata';
const client = new InfluxDB({ url: 'http://45.76.168.167:8086/', token: token });
const queryApi = client.getQueryApi(org);

/* Sqlite3 */
const table = 'config';


module.exports = {
    token,
    org,
    bucket,
    client,
    queryApi,
    table
};
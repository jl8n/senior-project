
## Problem
The wasabi plant is a difficult plant to grow because of how specific the growing conditions must be. As a result: real, fresh wasabi is rare and expensive.

## Solution
Wasabi might be difficult to cultivate on a large-scale, but a personal supply can be grown from home if the growing conditions are tightly controlled. Therefore, it's possible to build a system to monitor, report, and potentially self-correct conditions such as temperature, humidity, and air-flow in a controlled environment.

Ideally, a completely climate-controlled container can be built to grow the wasabi in, including mechanics to automate things like scheduled watering. However, a less-ambitious system could be built that could simply monitor the growing conditions and alert the grower about things like temperature going outside acceptable ranges.

## Technical Overview
Sensors for conditions like temperature, humidity, and light conditions will be hooked up to an ESP32 microcontroller, which will use those sensors to log growing data and send it to a Raspberry Pi over wi-fi. The Raspberry Pi will then sort the data and log it to an external MongoDB database using Python 3.8.

A web app will then be used to allow real-time monitoring of the data. Node.js 14 will be used to create a REST API that will query the database and respond to requests using Express. The WebSocket API will be used to deliver consistent real-time updates to the frontend.

A Progressive Web App will be built for the frontend, so users can install the web app to their phone and receive real-time push notifications about growing alerts. Vue 3 will be used as the front-end framework.

I've used a Raspberry Pi as a webserver before, but I don't have any experience with microcontrollers, hardware sensors, or sending data over wi-fi without high-level tools like scp. I've completed WEB 4300, so I have experience with MongoDB, Node, and Vue, but I plan on using Vue single-file components, and compiling the various frontend files using Webpack, which were things not taught in Web 4200, and are things that I'm only loosely familiar with. I also have no experience in developing a Progressive Web App, which has strict requirements to allow mobile functionality like notifications.

## Milestone List
### Milestone 1
Order necessary hardware components. Get Webpack and Vue single-file components working so the frontend can be compiled. Get a Node server to pull data from a database and push it to a webserver in JSON format. Wireframe the frontend.

### Milestone 2
Get the sensors working with the ESP32 microcontroller. Finish most of the frontend design, and get most of the frontend functionality working with the Node server using dummy data in the database. 

### Milestone 3
Get the microcontroller to send data to the Raspberry Pi, and then use Python to log the data to the database. Display the data asynchronously in the frontend using Node and Express. Make the frontend mobile friendly. 

### Milestone 4
Get Progressive Web App functionality working with the frontend, and use  Google Lighthouse to verify compliance. Deliver the data from the database in real-time using WebSockets. Add fields in the database for user-configured appropriate ranges for the sensor data. 

### Milestone 5
Develop a system on the Raspberry Pi to ensure the sensor data is consistently within appropriate ranges, otherwise, alert the grower via email and/or notification. Get push notifications working with the PWA. Squash unsolved UI bugs. If time allows, implement an automated watering system using a soil humidity sensor, solenoid valve, and water reservoir.

## Validation Plan
Ensure the sensors are reporting accurate data by comparing them to analog sensors where possible. Stress-test the system by artificially stimulating the sensors, like blowing hot air on the temperature sensor, to ensure the system responds appropriately. 



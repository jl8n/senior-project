import VueApexCharts from "vue3-apexcharts";
import { createApp, App } from 'vue';

const app = createApp();
app.use(VueApexCharts);
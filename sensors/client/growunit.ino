
#include <Arduino.h>
#include <sys/time.h>
#include <ArduinoJson.h>
#include <WiFiManager.h>
#include <WiFi.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;
float temperature, humidity, pressure;

void setup()
{
    Serial.begin(9600);
    delay(100);
    bool status = bme.begin(0x76);

    if (!status)
    {
        Serial.println("BME280 not found. Rebooting...");
        ESP.restart();
    }

    WiFi.mode(WIFI_STA);
    WiFiManager wifiManager;

    bool res;
    res = wifiManager.autoConnect("ESP32", "88888888"); // password protected ap

    if (!res)
    {
        Serial.println("Failed to connect");
        // ESP.restart();
    }
}

void loop()
{
    // construct JSON for http body
    const int capacity = JSON_OBJECT_SIZE(6);
    StaticJsonDocument<capacity> packet;
    packet["plant"] = 2; // needs to be unique per ESP32 board
    packet["temperature"] = bme.readTemperature();
    packet["humidity"] = bme.readHumidity();
    packet["pressure"] = bme.readPressure();
    packet["light"] = 0.50;

    Serial.println(bme.readTemperature());
    Serial.println(bme.readHumidity());
    Serial.println(bme.readPressure());

    WiFiClient client;
    client.connect("192.168.1.240", 8000);

    client.println("POST /measurements HTTP/1.1");

    // Send the HTTP headers
    client.println("Host: 192.168.1.240");
    client.print("Content-Length: ");
    client.println(measureJson(packet));
    client.println("Content-Type: application/json");
    client.println();              // Terminate headers with a blank line
    serializeJson(packet, client); // Send JSON document in body
    client.println("Connection: close");
    delay(60000);
}
#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

readonly SOURCE_PATH=./target/release/rust-server
readonly TARGET_HOST=pi
readonly TARGET_PATH=/var/www/autowasabi/sensors/rust-server/
readonly TARGET_ARCH=armv7-unknown-linux-gnueabihf

cargo build --release --target=${TARGET_ARCH}
scp ${SOURCE_PATH} pi:${TARGET_PATH} 
ssh -t ${TARGET_HOST} ${TARGET_PATH}
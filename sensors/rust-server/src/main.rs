#[macro_use] extern crate rocket;

use rocket_contrib::json::Json;
use serde::Deserialize;
use std::time::{UNIX_EPOCH, SystemTime};
use reqwest;


// TODO make every json key optional (Option<> / Request<>?)
#[derive(PartialEq, Debug, Deserialize)]
struct Packet {
    plant: u16,
    temperature: f32,
    humidity: f32,
    pressure: f32,
    light: f32
}

struct DBconfig {
    host: &'static str,
    port: u16,
    org: &'static str,
    bucket: &'static str,
    token: &'static str
}


async fn db_insert(db: DBconfig, data: Json<Packet>) -> Result<(), reqwest::Error> {
    let timestamp = SystemTime::now().duration_since(UNIX_EPOCH).expect("time went back");
    let timestamp = timestamp.as_secs() as u32;
    let db_uri: String = format!(
        "http://{}:{}/api/v2/write?org={}&bucket={}&precision=s",
        db.host, db.port, db.org, db.bucket
    );

    /* format measurement data to InfluxDB line protocol */
    let formatted_data: String = format!(
        "{0}\n{1}\n{2}\n{3}",
        format!("temperature,plant={} value={} {}", data.plant, data.temperature, timestamp),
        format!("humidity,plant={} value={} {}", data.plant, data.humidity, timestamp),
        format!("pressure,plant={} value={} {}", data.plant, data.pressure, timestamp),
        format!("light,plant={} value={} {}", data.plant, data.light, timestamp),
    );

    println!("fomatted data: {}", formatted_data);

    /* http POST request to InfluxDB server */
    let client = reqwest::Client::new();
    let res = client.post(&db_uri)
        .header("Authorization", db.token)
        .body(formatted_data)
        .send()
        .await?;
    
    println!("Status: {}", res.status());
    let body = res.text().await?;
    println!("Body:\n\n{}", body);
    Ok(())
}

#[post("/measurements", format = "json", data = "<body>")]
async fn insert_data(body: Json<Packet>) -> String {
    
    let db: DBconfig = DBconfig {
        host: "45.76.168.167",  // InfluxDB server
        port: 8086,
        org: "e3455fb6e26d89b3",
        bucket: "d205d63ad2dac8ab",
        token: "Token 86F7MkXKpbZDGm9pe7tPWfO1yWd4dpqWdD3NV3wMmMhj_z0I45dYEq2tsxCAqSsRzksfNLdFZxFfPY38XHSW-w=="
    };
    db_insert(db, body).await.expect("How did this happen?");
    format!("Successfully inserted measurement data!")
}

#[get("/")]
async fn get_test() -> String {
    format!("GET /\n\nAvailable:\n    POST /measurements\n")
    
}

#[launch]fn rocket() -> _ {
    rocket::ignite().mount("/", routes![get_test, insert_data])
}
